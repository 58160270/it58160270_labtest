CREATE VIEW StudentAdvise AS 
	SELECT Student.StudentID,Student.StudentName,Student.StudentSurname,Department.DeptName,Advisor.AdvisorName,Advisor.AdvisorSurname
	FROM Student
	INNER JOIN (Department,Advisor)
	ON (Student.DeptID = Department.DeptID AND Student.AdvisorID=Advisor.AdvisorID);


